package com.developer.renato.snake.infrastructure.enuns;

public enum Direction {
    North,
    East,
    South,
    West
}

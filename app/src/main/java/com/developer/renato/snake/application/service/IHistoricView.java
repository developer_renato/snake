package com.developer.renato.snake.application.service;

public interface IHistoricView {
    void showSucessHistoric();
    void showErrorHistoric(int resId);
}

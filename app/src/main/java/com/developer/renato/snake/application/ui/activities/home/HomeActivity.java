package com.developer.renato.snake.application.ui.activities.home;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import com.developer.renato.snake.R;
import com.developer.renato.snake.application.service.IHomeView;
import com.developer.renato.snake.application.service.home.presenter.HomePresenterImpl;
import com.developer.renato.snake.application.service.home.presenter.IHomePresenter;
import com.developer.renato.snake.application.ui.view.SnakeView;
import com.developer.renato.snake.infrastructure.enuns.Direction;
import com.developer.renato.snake.infrastructure.enuns.GameState;
import com.developer.renato.snake.infrastructure.helper.BaseActivity;

public class HomeActivity  extends BaseActivity implements IHomeView, View.OnTouchListener {

    private IHomePresenter presenter;

    private SnakeView snakeView;
    private  final Handler handler = new Handler();
    private final long updateDelay = 500;
    private float prevX, prevY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        snakeView = findViewById(R.id.snakeView);
        snakeView.setOnTouchListener(this);

        startUpdateHandler();

        presenter = new HomePresenterImpl(this);
        if (this.hasConnectivity(true)){
            this.loading(getResources().getString(R.string.loading));
            presenter.prepareSnake();
        }else {
            this.showErrorAlertWithMessage(R.string.mensagem_erro_problemas_conexao, close->{
                finish();
            });
        }
    }

    @Override
    public void showSucessSnake() {
        this.loading(getResources().getString(R.string.loading));
        this.hideLoading();
    }

    @Override
    public void showErrorSnake(int message) {
        this.hideLoading();
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton(R.string.dialog_neutral_button, (dialogInterface, i) -> {
                    finish();
                })
                .show();
    }

    private void startUpdateHandler(){
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                presenter.getHomeService().upDate();
                if(presenter.getHomeService().getCurrentGameState() == GameState.Running){
                    handler.postDelayed(this, updateDelay);

                }
                if(presenter.getHomeService().getCurrentGameState() ==GameState.Lost){
                    finishGame();
                }
                snakeView.setSnakeViewMap(presenter.getHomeService().getMap());
                snakeView.invalidate();
            }
        }, updateDelay);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                prevX  = event.getX();
                prevY  = event.getY();
                break;
            case MotionEvent.ACTION_UP:
                float newX = event.getX();
                float newY = event.getY();

                //Calculate
                if(Math.abs(newX - prevX)>Math.abs(newY -prevY)){
                    // LEFT - RIGHT direction
                    if(newX > prevX){
                        //RIGHT
                        presenter.getHomeService().updateDirection(Direction.East);
                    }else{
                        //LEFT
                        presenter.getHomeService().updateDirection(Direction.West);
                    }
                }else{
                    // UP - DOWN  direction
                    if(newY > prevY){
                        // DOWN
                        presenter.getHomeService().updateDirection(Direction.South);
                    }else{
                        // UP
                        presenter.getHomeService().updateDirection(Direction.North);
                    }
                }
                break;
        }
        return true;
    }

    public void finishGame(){
        new AlertDialog.Builder(this)
                .setMessage(R.string.str_home_finish_game)
                .setPositiveButton(R.string.dialog_button_restart, (dialogInterface, i) -> {
                    Intent intent = new Intent();
                    intent.setClass(this, this.getClass());
                    this.startActivity(intent);
                    this.finish();
                })
                .setNegativeButton(R.string.dialog_button_close, (dialogInterface, i) -> {
                    finish();
                })
                .show();
    }
}
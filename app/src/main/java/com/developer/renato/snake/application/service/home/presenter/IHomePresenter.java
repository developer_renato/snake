package com.developer.renato.snake.application.service.home.presenter;

import com.developer.renato.snake.application.service.home.service.HomeServiceImpl;

public interface IHomePresenter {
    void prepareSnake();
    HomeServiceImpl getHomeService();
}

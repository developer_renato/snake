package com.developer.renato.snake.application.service.home.presenter;

import com.developer.renato.snake.R;
import com.developer.renato.snake.application.service.IHomeView;
import com.developer.renato.snake.application.service.home.service.HomeServiceImpl;

public class HomePresenterImpl implements IHomePresenter {

    private IHomeView view;
    private HomeServiceImpl homeService = new HomeServiceImpl();

    public HomePresenterImpl(IHomeView view){
        this.view = view;
    }

    @Override
    public void prepareSnake() {
        try {
            if(this.homeService.preparePlay()){
                this.view.showSucessSnake();
            }else{
                this.view.showErrorSnake(R.string.str_home_error);
            }
        }catch (Exception e){
            e.getMessage().toString();
        }
    }

    @Override
    public HomeServiceImpl getHomeService() {
        return homeService;
    }

}

package com.developer.renato.snake.application.service;

public interface IHomeView {
    void showSucessSnake();
    void showErrorSnake(int resId);
}

package com.developer.renato.snake.infrastructure.helper;
import android.app.Activity;
import android.content.Context;
import android.view.View;

public interface IBaseActivity {

    Context getContext();

    void showErrorAlertWithMessage(int message);
    void showErrorAlertWithMessage(int message, View.OnClickListener onClickListener);
    void showErrorAlertWithMessage(int message, View.OnClickListener positive, View.OnClickListener negative);
    void showAlertWithMessage(String message);

    boolean hasConnectivity(boolean showError);
    boolean hasConnectivity(boolean showError, View.OnClickListener listener);

    void loading();
    void loading(String message);
    void loading(String message, int style);

    void hideLoading();
    void close();

    void goActivity(Activity act);
}

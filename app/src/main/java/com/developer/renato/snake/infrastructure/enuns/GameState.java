package com.developer.renato.snake.infrastructure.enuns;

public enum GameState {
    Ready,
    Running,
    Lost
}

package com.developer.renato.snake.application.service.home.service;

import com.developer.renato.snake.domain.model.Coordinate;
import com.developer.renato.snake.infrastructure.enuns.Direction;
import com.developer.renato.snake.infrastructure.enuns.GameState;
import com.developer.renato.snake.infrastructure.enuns.TileType;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class HomeServiceImpl  implements IHomeService {
    public static final int GAME_WIDTH = 28;
    public static final int GAME_HEIGHT = 42;
    private List<Coordinate> walls = new ArrayList();
    private List<Coordinate> snake = new ArrayList();
    private List<Coordinate> apples = new ArrayList();
    private Random random = new Random();
    private boolean increaseTail = false;

    private Direction currentDirection = Direction.East;
    private GameState currentGameState = GameState.Running;

    @Override
    public boolean preparePlay() {
        if(applySnake() && applyWalls() && applyApples()){
            return true;
        }
        return false;
    }

    private Coordinate getSnakeHead(){
        return snake.get(0);
    }

    public void updateDirection(Direction newDirection){
        if(Math.abs(newDirection.ordinal() - currentDirection.ordinal()) %2 ==1){
            currentDirection = newDirection;
        }
    }

    public  void upDate(){
        //Update the snake
        switch (currentDirection){

            case North:
                UpdateSnake(0, -1);
                break;
            case East:
                UpdateSnake(1, 0);
                break;
            case South:
                UpdateSnake(0, 1);
                break;
            case West:
                UpdateSnake(-1, 0);
                break;
        }

        //check wall collison
        for(Coordinate w: walls){
            if(snake.get(0).equals(w)){
                currentGameState = GameState.Lost;
            }
        }

        //check self collison
        for(int i=1; i<snake.size(); i++){
            if(getSnakeHead().equals(snake.get(i))){
                currentGameState = GameState.Lost;
                return;
            }
        }

        //check apples
        Coordinate appleToRemove = null;
        for(Coordinate apple: apples){
            if(getSnakeHead().equals(apple)){
                appleToRemove = apple;
                increaseTail = true;
            }
        }

        if(appleToRemove != null){
            apples.remove(appleToRemove);
            applyApples();
        }
    }

    private void UpdateSnake(int x, int y){

        int newX = snake.get(snake.size()-1).getX();
        int newY = snake.get(snake.size()-1).getY();

        for(int i= snake.size()-1; i>0; i--){
            snake.get(i).setX(snake.get(i-1).getX());
            snake.get(i).setY(snake.get(i-1).getY());
        }

        if(increaseTail){
            snake.add(new Coordinate(newX, newY));
            increaseTail = false;
        }
        snake.get(0).setX(snake.get(0).getX() + x);
        snake.get(0).setY(snake.get(0).getY() + y);
    }

    private boolean applySnake(){
        try{
            snake.clear();
            snake.add(new Coordinate(7,7));
            snake.add(new Coordinate(6,7));
            snake.add(new Coordinate(5,7));
            snake.add(new Coordinate(4,7));
            snake.add(new Coordinate(3,7));
            snake.add(new Coordinate(2,7));
            return true;
        }catch (Exception e){
            e.getMessage().toString();
        }
       return false;
    }

    public TileType[][] getMap(){
        TileType[][] map = new TileType[GAME_WIDTH][GAME_HEIGHT];

        for(int x=0; x< GAME_WIDTH; x++){
            for(int y=0; y< GAME_HEIGHT; y++){
                map[x][y] = TileType.Nothing;
            }
        }

        for(Coordinate wall: walls){
            map[wall.getX()] [wall.getY()] = TileType.Wall;
        }

        for(Coordinate s: snake){
            map[s.getX()][s.getY()] = TileType.SnakeTail;
        }

        for(Coordinate a: apples){
            map[a.getX()][a.getY()] = TileType.Apple;
        }

        map[snake.get(0).getX()][snake.get(0).getY()] = TileType.SnakeHead;

        return map;
    }

    private boolean applyWalls(){
        try{
            //Top and bottom walls
            for(int x=0; x<GAME_WIDTH; x++){
                walls.add(new Coordinate(x,0));
                walls.add(new Coordinate(x,GAME_HEIGHT-1));
            }

            //Left and Right walls
            for(int y=1; y<GAME_HEIGHT; y++){
                walls.add(new Coordinate(0,y));
                walls.add(new Coordinate(GAME_WIDTH -1,y));
            }
            return true;
        }catch (Exception e){
            e.getMessage().toString();
        }
        return false;
    }

    public GameState getCurrentGameState(){
        return currentGameState;
    }

    private boolean applyApples(){
        try {
            Coordinate coordinate = null;
            boolean added = false;

            while (!added){
                int x = 1 + random.nextInt(GAME_WIDTH - 2);
                int y = 1 + random.nextInt(GAME_HEIGHT - 2);

                coordinate = new Coordinate(x, y);
                boolean collision = false;
                for(Coordinate s: snake){
                    if(s.equals(coordinate)){
                        collision = true;
                        //break;
                    }
                }

                for(Coordinate a: apples){
                    if(a.equals(coordinate)){
                        collision = true;
                        //break;
                    }
                }
                added = !collision;
            }
            apples.add(coordinate);
            return true;
        }catch (Exception e){
            e.getMessage().toString();
        }
        return false;
    }
}

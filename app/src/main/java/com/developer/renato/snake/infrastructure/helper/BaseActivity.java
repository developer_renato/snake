package com.developer.renato.snake.infrastructure.helper;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.developer.renato.snake.R;


public class BaseActivity extends AppCompatActivity implements IBaseActivity {

    ProgressDialog mDialog;

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showErrorAlertWithMessage(int message) {
        showErrorAlertWithMessage(message, null);
    }

    @Override
    public void showErrorAlertWithMessage(final int message, final View.OnClickListener onClickListener) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    new MaterialDialog.Builder(BaseActivity.this)
                            .limitIconToDefaultSize()
                            .content(message)
                            .positiveText(BaseActivity.this.getString(R.string.dialog_neutral_button))
                            .cancelable(false)
                            .positiveColor(BaseActivity.this.getResources().getColor(R.color.colorPrimary))
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    if (onClickListener != null) {
                                        onClickListener.onClick(null);
                                    }
                                }
                            })
                            .contentColor(ContextCompat.getColor(BaseActivity.this, R.color.color_label1))
                            .show();
                }
            });

        } catch (Exception exception) {
            //
        }
    }

    @Override
    public void showErrorAlertWithMessage(final int message, final View.OnClickListener positive, final View.OnClickListener negative) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    new MaterialDialog.Builder(BaseActivity.this)
                            .limitIconToDefaultSize()
                            .content(message)
                            .positiveText(BaseActivity.this.getString(R.string.dialog_neutral_button))
                            .negativeText("NÃO")
                            .cancelable(false)
                            .positiveColor(BaseActivity.this.getResources().getColor(R.color.colorPrimary))
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    if (positive != null) {
                                        positive.onClick(null);
                                    }
                                }
                            })
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    if (negative != null) {
                                        negative.onClick(null);
                                    }
                                }
                            })
                            .contentColor(ContextCompat.getColor(BaseActivity.this, R.color.color_label1))
                            .show();
                }
            });

        } catch (Exception exception) {
            //
        }

    }

    @Override
    public void showAlertWithMessage(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                new MaterialDialog.Builder(BaseActivity.this)
                        .limitIconToDefaultSize()
                        .content(message)
                        .positiveText(BaseActivity.this.getString(R.string.dialog_neutral_button))
                        .cancelable(false)
                        .positiveColor(BaseActivity.this.getResources().getColor(R.color.colorPrimary))
                        .contentColor(ContextCompat.getColor(BaseActivity.this, R.color.color_label1))
                        .show();
            }
        });
    }

    @Override
    public boolean hasConnectivity(boolean showError) {
        return hasConnectivity(showError, null);
    }

    @Override
    public boolean hasConnectivity(boolean showError, final View.OnClickListener listener) {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo == null || !cm.getActiveNetworkInfo().isAvailable() || !cm.getActiveNetworkInfo().isConnected()) {

            if(showError) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        new MaterialDialog.Builder(BaseActivity.this)
                                .limitIconToDefaultSize()
                                .content(BaseActivity.this.getResources().getString(R.string.mensagem_erro_problemas_conexao))
                                .positiveText(BaseActivity.this.getString(R.string.dialog_neutral_button))
                                .cancelable(false)
                                .positiveColor(BaseActivity.this.getResources().getColor(R.color.colorPrimary))
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        if (listener != null) {
                                            listener.onClick(null);
                                        }
                                    }
                                })
                                .contentColor(ContextCompat.getColor(BaseActivity.this, R.color.color_label1))
                                .show();

                    }
                });
            }
            return false;
        }
        return true;
    }

    @Override
    public void loading() {
        if(mDialog==null || !mDialog.isShowing()) {
            mDialog = new ProgressDialog(this, R.style.Alert_DialogStyle);
            mDialog.setTitle(getString(R.string.empty));
            mDialog.setMessage(getString(R.string.loading));
            mDialog.setIndeterminate(true);
            mDialog.setCancelable(false);
            mDialog.show();
        }
    }

    @Override
    public void loading(String message) {
        if(mDialog==null || !mDialog.isShowing()) {
            mDialog = new ProgressDialog(this, R.style.Alert_DialogStyle);
            mDialog.setTitle(getString(R.string.empty));
            mDialog.setMessage(message);
            mDialog.setIndeterminate(true);
            mDialog.setCancelable(false);
            mDialog.show();
        }
    }

    @Override
    public void loading(String message, int style) {
        if(mDialog==null || !mDialog.isShowing()) {
            mDialog = new ProgressDialog(this, style);
            mDialog.setTitle(getString(R.string.empty));
            mDialog.setMessage(message);
            mDialog.setIndeterminate(true);
            mDialog.setCancelable(false);
            mDialog.show();
        }
    }

    @Override
    public void hideLoading() {
        if(mDialog != null) {
            mDialog.dismiss();
            mDialog.cancel();
        }
    }

    @Override
    public void close() {
        finish();
    }

    @Override
    public void goActivity(Activity act) {
        Intent it = new Intent();
        it.setClass(getApplicationContext(), act.getClass());
        it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(it);
        finish();
    }
}

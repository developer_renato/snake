package com.developer.renato.snake.infrastructure.enuns;

public enum TileType {
    Nothing,
    Wall,
    SnakeHead,
    SnakeTail,
    Apple
}
